import 'package:bytebank/database/dao/contact_dao.dart';
import 'package:bytebank/models/contact.dart';
import 'package:bytebank/screens/contact_form.dart';
import 'package:flutter/material.dart';

class ContactsList extends StatefulWidget {

  @override
  _ContactListState createState() => _ContactListState();
}

class _ContactListState extends State<ContactsList> {
  final List<Contact> contacts = List();
  final ContactDao _contactDao = ContactDao();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Contacts"),
      ),
      // Informamos que o "FutureBuilder" somente aceitará uma lista de contato
      body: FutureBuilder<List<Contact>>(
        // O dado inicial será uma lista vazia, evitando assim erros na tela
        initialData: List(),
        // A propriedade "future" indica o que será executado
        future: _contactDao.findAll(),
        // A referência de snapshot contém todas as informações  da execução do future que estamos
        // executando e com o qual teremos acesso à lista de contatos
        builder: (context, snapshot) {
          // O snapshot.connectionState serve para que possamos identificar os estagios do future
          // a fim de tomar ações em cada um deles
          switch (snapshot.connectionState) {
          /**
           * No primeiro caso, ConnectionState.none, o Future ainda não foi executado. Nesse cenário, é costumeiro
           * colocar algum tipo de widget que permite um clique ou outro tipo de ação, e que dê início ao Future,
           * ermitindo a execução das demais ações. Como esse não é o nosso cenário, manteremos esse caso vazio.
           */
            case ConnectionState.none:
              break;
          /**
           * O segundo caso, ConnectionState.waiting, é um estado no qual verificaremos se o Future ainda está carregando
           * e não foi finalizado. Ou seja, aqui poderemos devolver nosso progresso CircularProgressIndicator()
           * (e todo o bloco de código que o acompanha).
           */
            case ConnectionState.waiting:
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    Text("Loading"),
                  ],
                ),
              );
              break;
          /**
           *  O ConnectionState.active ocorre quando nosso snapshot possui um dado disponível, mas o
           *  Future ainda não foi finalizado. Isso acontece quando utilizamos outra referência, conhecida
           *  como stream, que trabalha trazendo pedaços de um carregamento assíncrono, por exemplo no caso
           *  do progresso de um download. Como esse não é o nosso caso, manteremos esse caso vazio.
           */
            case ConnectionState.active:
              break;
            case ConnectionState.done:
            // Estamos indicando que o tipo esperado do snapshot.data (que contém os dados obtidos pela propriedade
            // "future") será uma lista de contatos
              final List<Contact> contacts = snapshot.data;
              return ListView.builder(
                itemBuilder: (context, index) {
                  /**
                   * como se fosse um for, o index da função do itemBuilder
                   * é passado como referência para que o contacts saiba qual é o dado
                   * a ser exibido na tela. O contact recebe então um "contacts[index]"
                   * por vez e envia para o widget "_ContactItem" que monta os cards
                   */
                  final Contact contact = contacts[index];
                  return _ContactItem(contact);
                },
                // Necessário para construção da listagem
                itemCount: contacts.length,
              );
              break;
          }
          return Text("Unknown error");
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => ContactForm(),
            ),
          );
        },
        child: Icon(Icons.add),
      ),
    );
  }
}

class _ContactItem extends StatelessWidget {
  final Contact contact;

  _ContactItem(this.contact);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
      child: ListTile(
        title: Text(
          contact.name,
          style: TextStyle(fontSize: 24.0),
        ),
        subtitle: Text(
          contact.accountNumber.toString(),
          style: TextStyle(fontSize: 16.0),
        ),
      ),
    );
  }
}
