import 'package:bytebank/screens/contacts_list.dart';
import 'package:flutter/material.dart';

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dashboard"),
      ),
      body: Column(
        // O mainAxisAlignment é responsável pelo alinhamento vertical. O spaceBetween separou nos dois extremos da tela os conteúdos dentro do body
        mainAxisAlignment: MainAxisAlignment.spaceBetween,

        // O crossAxisAlignment é responsável pelo alinhamento horizontal. O start posicionou os elementos nos seus respectivos "Start", ou seja, do lado esquerdo
        crossAxisAlignment: CrossAxisAlignment.start,

        // children serve para quando se quer colocar widgets dentro de outro
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.asset('images/bytebank_logo.png'),
          ),
          Padding(
              padding: const EdgeInsets.all(8.0),
              child: Material(
                color: Theme.of(context).primaryColor,
                child: InkWell(
                  // InkWell é o equivalente ao "GestureDetector" do Flutter, porém ele é dedicado ao Material. Ele permite colocar ações de movimentos em componentes que naturalmente nao possuem, como é o caso de agora, onde vamos atribuir o evento de "tap" em um container
                  onTap: () {
                    // Trecho de código para fazer a navegação de uma tela para outra
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => ContactsList(),
                    ));
                  },
                  child: Container(
                    padding: EdgeInsets.all(8),
                    height: 100,
                    width: 150,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          Icons.people,
                          color: Colors.white,
                          size: 24.0,
                        ),
                        Text(
                          "Contacts",
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )),
        ],
      ),
    );
  }
}
