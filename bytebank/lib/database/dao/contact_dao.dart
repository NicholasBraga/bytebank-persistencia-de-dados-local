import 'package:bytebank/models/contact.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:bytebank/database/app_database.dart';

class ContactDao {

  static const String _tableName = 'contacts';
  static const String _id = 'id';
  static const String _name = 'name';
  static const String _accountNumber = 'account_number';
  static const String tableSql = 'CREATE TABLE $_tableName('
      '$_id INTEGER PRIMARY KEY, '
      '$_name TEXT, '
      '$_accountNumber INTEGER)';

  Future<int> save(Contact contact) async {
    // Recuperamos a instancia do banco de dados
    final Database db = await getDatabase();

    // Preparamos os dados para serem inseridos
    Map<String, dynamic> contactMap = _toMap(contact);
    return db.insert(_tableName, contactMap);
  }

  /**
   * Cria um mapa de contatos (mapa é similar a listas)
   */
  Map<String, dynamic> _toMap(Contact contact) {
     final Map<String, dynamic> contactMap = Map();
    contactMap[_name] = contact.name;
    contactMap[_accountNumber] = contact.accountNumber;
    return contactMap;
  }

  Future<List<Contact>> findAll() async {
    // Recuperamos a instância do banco de dados
    final Database db = await getDatabase();

    /**
     * Nossa query() espera uma lista com um mapa de chave String e tipo dinâmico. Sendo assim, adicionaremos o await e
     * atribuiremos o retorno da chamada a uma variável result com o tipo especificado. Em seguida, de posse do result,
     * executaremos a mesma iteração que fazíamos anteriormente, substituindo os pontos necessários.
     * Ao invés de maps, vamos iterar pela variável result que acabamos de criar, e chamaremos cada item encontrado pelo
     * nome row (linha), que tem mais relação com as tabelas do banco de dados.
     */
    final List<Map<String, dynamic>> result = await db.query(_tableName);

    /**
     * Assim, faremos basicamente a mesma operação que antes: pegaremos o resultado, criaremos uma nova lista, iteraremos por
     * cada uma das linhas encontradas no resultado, criaremos um contato a partir das informações dessas linhas e adicionaremos
     * esse contato à lista. Por fim, retornaremos a lista de contatos.
     */
    List<Contact> contacts = _toList(result);
    return contacts;
  }

  /**
   * Recebe uma lista de contatos e cria objetos de contatos
   */
  List<Contact> _toList(List<Map<String, dynamic>> result) {
    final List<Contact> contacts = List();
    for (Map<String, dynamic> row in result) {
      final Contact contact = Contact(
        row[_id],
        row[_name],
        row[_accountNumber],
      );
      contacts.add(contact);
    }
    return contacts;
  }
}
